package de.prisemut.mywe;

import org.bukkit.entity.Player;

public class Messages {

    private Player player;
    private String prefix = "§7[§3MyWE§7] ";

    public Messages(Player player) {
        this.player = player;
    }

    public void sendMessage(String msg) {
        player.sendMessage(prefix + msg);
    }

    public void sendNoPerm() {
        sendMessage("You don`t have the permissions for this!");
    }
}
