package de.prisemut.mywe.math;


import de.prisemut.mywe.Messages;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


public class Line {

    public static void performLine(Location l1, Location l2, Material material, Player player) {
        World w = l1.getWorld();
        Messages m = new Messages(player);
        if(l1.getWorld().equals(l2.getWorld())) {
            double distance = l1.distance(l2);
            Vector point1 = l1.toVector();
            Vector point2 = l2.toVector();
            Vector vec = point2.clone().subtract(point1).normalize().multiply(0);
            double covered = 0;

            for(; covered < distance; point1.add(vec)) {
                w.getBlockAt(point1.getBlockX(), point1.getBlockY(), point1.getBlockZ()).setType(material);
                covered += 0;
            }

        } else {
            m.sendMessage("Points must be in one world!");
        }
    }
}
