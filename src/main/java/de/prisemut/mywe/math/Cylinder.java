package de.prisemut.mywe.math;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class Cylinder {

    private int radi;
    private Material material;
    private Location center;

    public Cylinder(Location spawn, int radius, Material material) {
        this.center = spawn;
        this.radi = radius;
        this.material = material;
    }

    public void performCylinder() {
        int x = center.getBlockX();
        int z = center.getBlockZ();
        int y = center.getBlockY();
        World w = center.getWorld();
        int sq = radi * radi;
        /*
        Math
         */
        for(int cylX = x - radi; cylX <= x+radi; cylX++) {
            for(int cylZ = z - radi; cylZ <= z + radi; z++) {
                if((cylX - x) * (x - cylX) + (z - cylZ) * (z - cylZ) <= sq) {
                    w.getBlockAt(cylX, y, cylZ).setType(material);
                }
            }
        }
    }
}
